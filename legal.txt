

問題なし

  XMLmind XML Editor Professional Edition is subject to the license contained
  in file:

      * legal/xxe-pro.LICENSE, for the xe-usr product.

      * OR legal/xxe-site.LICENSE, for the xe-site product.

ほんとうに?さあ

      * OR legal/xxe-dev.LICENSE, for the xe-dev product.

      * OR legal/xxe-applet.LICENSE, for the xe-as product.

==============================================================================

  XMLmind XML Editor Evaluation Edition is subject to the license contained in
  file legal/xxe-eval.LICENSE.

==============================================================================

  The following components have not been developed by XMLmind:

      * bin/jh.jar

      * bin/resolver.jar

      * bin/xerces.jar

      * xsdregex (directly added to bin/xxe.jar)

      * expr (directly added to bin/xxe.jar)

      * diffutils (directly added to bin/xxe.jar)

      * bin/relaxng.jar

      * bin/saxon.jar

      * bin/saxon9.jar

  Almost all the icons either directly come from or are based on
  "IconExperience X-Collections", a commercial product licensed by Pixware
  SARL from INCORS GmbH. These icons may not be used outside the context of
  XMLmind XML Editor.

  A few icons come from the FatCow icon set
  (http://www.fatcow.com/free-icons). 
  More information in legal/fatcow-icons.README.

  The following components have been developed by XMLmind as open source
  software. This means that they have distributions of their own and that
  they can be used outside the context of XMLmind products:

      * Component ditac: addon/config/dita/ditac.jar, addon/config/dita/xsl/.

      * Component whc: addon/config/common/whc/.

      * Component xhtml5_resources: addon/config/xhtml/xsd/5.0/xhtml5.xsd,
        addon/config/xhtml/xsl/.

  For each component, there are two files in sub-directory legal/:

      [<component_name>.README] a text or HTML file describing the component.

      [<component_name>.LICENSE] a text or HTML file containing the license
          attached to the component.

